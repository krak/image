<?php

namespace Krak\Image;

use InvalidArgumentException;

final class ImageTypes
{
    const JPEG = 0x0;
    const PNG = 0x1;

    public static function toContentType($type)
    {
        switch ($type) {
            case self::JPEG:
                return 'image/jpeg';
            case self::PNG:
                return 'image/png';
        }

        throw new InvalidArgumentException('invalid type constant given');
    }
}
