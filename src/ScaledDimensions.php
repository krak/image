<?php

namespace Krak\Image;

class ScaledDimensions implements Dimensions
{
    private $x;
    private $y;
    private $width;
    private $height;
    private $scale;

    public function __construct($x, $y, $w, $h, $scale = 1.0)
    {
        $this->x = $x;
        $this->y = $y;
        $this->width = $w;
        $this->height = $h;
        $this->scale = $scale;
    }

    public function getX()
    {
        return round($this->x * $this->scale);
    }
    public function getY()
    {
        return round($this->y * $this->scale);
    }
    public function getWidth()
    {
        return round($this->width * $this->scale);
    }
    public function getHeight()
    {
        return round($this->height * $this->scale);
    }
}
