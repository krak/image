<?php

namespace Krak\Image;

use Imagick,
    ImagickPixel;

class ImagickImage implements Image
{
    private $img;

    public function __construct(Imagick $img)
    {
        $this->img = $img;
    }

    public function getImageResource()
    {
        return $this->img;
    }

    public function crop(Dimensions $d)
    {
        $this->img->cropImage($d->getWidth(), $d->getHeight(), $d->getX(), $d->getY());
    }

    public function getWidth()
    {
        return $this->img->getImageWidth();
    }

    public function getHeight()
    {
        return $this->img->getImageHeight();
    }

    public function getType()
    {
        if (strcasecmp($this->img->getImageFormat(), 'png') == 0) {
            return ImageTypes::PNG;
        }
        if (preg_match('/(jpg|jpeg)/i', $this->img->getImageFormat())) {
            return ImageTypes::JPEG;
        }

        return -1;
    }

    public function compressJpeg($quality)
    {
        $this->img->setImageFormat('jpeg');
        $this->img->setImageCompression(Imagick::COMPRESSION_JPEG);
        $this->img->setImageCompressionQuality($quality);
    }

    public function getBinaryString()
    {
        return $this->img->getImageBlob();
    }

    public function destroy()
    {
        $this->img->clear();
    }

    public function autoRotate()
    {
        $orientation = $this->img->getImageOrientation();

        /* thank you stack overflow - http://stackoverflow.com/questions/4266656/how-to-stop-php-imagick-auto-rotating-images-based-on-exif-orientation-data */
        switch($orientation)
        {
            case imagick::ORIENTATION_BOTTOMRIGHT:
                $this->img->rotateimage(new ImagickPixel('none'), 180);
                break;
            case imagick::ORIENTATION_RIGHTTOP:
                $this->img->rotateimage(new ImagickPixel('none'), 90);
                break;
            case imagick::ORIENTATION_LEFTBOTTOM:
                $this->img->rotateimage(new ImagickPixel('none'), -90);
                break;
        }

        /* Now that it's auto-rotated, make sure the EXIF data is correct in case
           the EXIF gets saved with the image. */
        $this->img->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
    }

    public function __clone()
    {
        $this->img = clone $this->img;
    }
}
