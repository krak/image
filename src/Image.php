<?php

namespace Krak\Image;

/**
 * An interface for an image
 */
interface Image
{
    /**
     * Return the underlying image resource
     */
    public function getImageResource();
    public function crop(Dimensions $d);
    public function getWidth();
    public function getHeight();
    /**
     * return the image in a binary string format
     */
    public function getBinaryString();
    public function destroy();
    public function getType();
    public function compressJpeg($quality);
    /**
     * Check the EXIF data and auto rotate the image to the proper orientation
     */
    public function autoRotate();
    public function __clone();
}
