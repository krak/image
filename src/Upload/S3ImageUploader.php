<?php

namespace Krak\Image\Upload;

use Aws\S3\S3Client,
    Krak\Image\Image,
    Krak\Image\ImageTypes;

class S3ImageUploader implements ImageUploader
{
    private $client;
    private $bucket;

    public function __construct(S3Client $client, $bucket)
    {
        $this->client = $client;
        $this->bucket = $bucket;
    }

    public function uploadImage(Image $image, $path)
    {
        $this->client->putObject([
            'Bucket' => $this->bucket,
            'Key' => $path,
            'Body' => $image->getBinaryString(),
            'ContentType' => ImageTypes::toContentType($image->getType()),
        ]);
    }
}
