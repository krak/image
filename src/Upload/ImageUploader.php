<?php

namespace Krak\Image\Upload;

use Krak\Image\Image;

interface ImageUploader
{
    public function uploadImage(Image $image, $path);
}
