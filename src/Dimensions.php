<?php

namespace Krak\Image;

interface Dimensions
{
    public function getX();
    public function getY();
    public function getWidth();
    public function getHeight();
}
