<?php

namespace Krak\Image;

class StubImage implements Image
{
    private $resource;
    private $width;
    private $height;
    private $binary_string;
    private $type;

    public function __construct($resource, $width, $height, $binary_string, $type)
    {
        $this->resource = $resource;
        $this->width = $width;
        $this->height = $height;
        $this->binary_string = $binary_string;
        $this->type = $type;
    }

    public function getImageResource()
    {
        return $this->resource;
    }

    public function crop(Dimensions $d)
    {
        /* do nothing */
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    /**
     * {@inheritdoc}
     */
    public function getBinaryString()
    {
        return $this->binary_string;
    }

    public function destroy()
    {
        /* do nothing */
    }

    public function getType()
    {
        return $this->type;
    }

    public function compressJpeg($quality)
    {
        /* do nothing */
    }

    /**
     * {@inheritdoc}
     */
    public function autoRotate()
    {
        /* do nothing */
    }

    public function __clone()
    {
        /* do nothing */
    }
}
