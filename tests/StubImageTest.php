<?php

namespace Krak\Tests;

use Krak\Image;

class StubImageTest extends TestCase
{
    public function testConstruct()
    {
        $image = new Image\StubImage('resource', 100, 200, 'binary_string', Image\ImageTypes::JPEG);
        $this->assertInstanceOf(Image\StubImage::class, $image);
        return $image;
    }

    /**
     * @depends testConstruct
     */
    public function testGetImageResource($image)
    {
        $this->assertEquals('resource', $image->getImageResource());
    }
    /**
     * @depends testConstruct
     */
    public function testGetWidth($image)
    {
        $this->assertEquals(100, $image->getWidth());
    }
    /**
     * @depends testConstruct
     */
    public function testGetHeight($image)
    {
        $this->assertEquals(200, $image->getHeight());
    }
    /**
     * @depends testConstruct
     */
    public function testGetBinaryString($image)
    {
        $this->assertEquals('binary_string', $image->getBinaryString());
    }
    /**
     * @depends testConstruct
     */
    public function testGetType($image)
    {
        $this->assertEquals(Image\ImageTypes::JPEG, $image->getType());
    }
}
