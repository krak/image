# Image

A library for image manipulation

## Installation

The best way to install Krak\Image is via composer. The library lives on bighead's gitlab servers, so you'll need to add the repository in the composer.json.

```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.bighead.net:bighead/krak-image.git"
        }
    ],
    "require": {
        "krak/image": "dev-master"
    }
}
```